import kotlin.random.Random

class Alien(val name: String, var health: Int) {

    // additional random alien
    companion object {
        // List of predefined alien types
        val alienTypes = listOf(
            Alien("Xenomorph", 40),
            Alien("Gundar", 30),
            Alien("Cylon", 50)
        )

        // Function to generate a random alien
        fun generateRandomAlien(): Alien {
            return alienTypes[Random.nextInt(alienTypes.size)]
        }
    }

    fun attack (player: Player): Int {
        val damage = Random.nextInt(5,10)
        player.health -= damage
        println("*** \n $name attacks ${player.name} for $damage")

        return damage
    }
}