import kotlin.random.Random

class Player(val name: String, var health: Int, var credits: Int) {
    fun attack(alien: Alien): Int {
        val damage: Int = Random.nextInt(10, 15)
        alien.health -= damage
        println("*** \n $name deals $damage damage to ${alien.name}!")
        return damage
    }

    fun heal() {
        val healUp: Int = Random.nextInt(10, 20)
        this.health += healUp
        println("+++ \n You heal yourself for $healUp HP")
    }
}