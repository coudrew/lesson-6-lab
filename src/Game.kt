import kotlin.random.Random

object Game{
    lateinit var player: Player

    fun start(){
        val numberOfAliens = Random.nextInt(3, 6)
        println("\n Welcome to the Alien Game, please enter your name: ")
        val playerName = readLine() ?: "Player"
        val player = Player(playerName, 50, 10)
        var currentIndex = 0
        while (currentIndex <= numberOfAliens){
            val alien = Alien.generateRandomAlien()
            println("\n A ${alien.name} appears!")
            while(player.health > 0 && alien.health > 0){
                println("\n Please choose an option 1) attack 2) heal ")
                var option = readLine()?.toIntOrNull() ?: 1
                if (option == 1){
                    player.attack(alien)
                } else {
                    player.heal()
                }
                if (alien.health > 0) {
                    alien.attack(player)
                }
                println("--- \n ${player.name} health is ${player.health} \n ${alien.name} health is ${alien.health} \n ---")
            }
            if(player.health > 0){
                player.credits += 20
                println("You have defeated the ${alien.name} and claim 20 credits \n")
            } else {
                println("You are defeated, Game over! \n")
                break
            }
            currentIndex++
        }
        if(currentIndex > numberOfAliens) println("Congratulations you have defeated all aliens \n")
    }
}